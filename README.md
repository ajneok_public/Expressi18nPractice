# Express + i18n + cookie 練習

## 套件
* 使用 [i18n](https://github.com/mashpie/i18n-node) 套件

## libraries/i18n.js
* 做 middleware 部分，判斷網址是否有帶 lang 的 query 並檢查是否是允許的語系，有就修改語系(回寫到 cookie 並重整頁面)

## index.js
* 主進入點用 `app.use(i18n)` 導入 i18n 的 middleware 讓所有 request 都先過 i18n middleware 的檢查

## locales/*.json
* 翻譯檔，key value 形式，可參考套件官網
